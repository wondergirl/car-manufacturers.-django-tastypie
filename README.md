# README #

Test Django + Tastypie application. 

### How do I get set up? ###

* Run python manage.py runserver
* API endpoint: /api/v1
* Credentials: username: ann
               password: 123 

Available API endpoints:

* GET /api/v1/car

* GET /api/v1/car/id

* POST /api/v1/car

* GET /api/v1/color

* GET /api/v1/manufacturer


### What you propose if cars will be very big table? ###
* Partitioning over cluster
* Probably horizontal partitioning could work by splitting the table by manufacturers/cost/engine capacity
* Some fields which would be used often in searching could be indexed
* Different database management system could be used

### What api could be cached and how? ###
From my point of view, in this case the color data could be cached using expiration method because as far as I
understood, the color data wouldn't change frequently and unpredictably. Car and manufacturer data could be cached as 
well, but using validation strategy. This information would be added and accessed frequently by the many users so
expiration strategy wouldn't work correctly. Django's cache framework could be used.

### How can you enable rate limit for api and how to test it? ####
Using throttling - throttle = BaseThrottle(throttle_at=1, timeframe=10) - for example, and then executing get requests
(script in test_throttling.py). After allowed period of time, the script shows "ConnectionError:Max retries exceeded 
with url" error. This can be also set at the server side by setting up the max_connection
(ngx_http_limit_req_module(nginx) or mod_evasive(apache), for example)

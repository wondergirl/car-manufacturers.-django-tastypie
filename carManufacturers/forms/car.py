__author__ = 'ann'

from django import forms

from carManufacturersApp.models import Car
from carManufacturersApp.models import Color
from carManufacturersApp.models import Manufacturer
from carManufacturers.input_helpers import clean_input


class AddCarForm(forms.Form):
    name = forms.CharField(max_length=100, required=True, help_text='Required')
    cost = forms.FloatField(required=True, help_text='Required', min_value=1)
    image = forms.ImageField(required=False)
    engine_capacity = forms.IntegerField(required=True, help_text='Required', min_value=1)
    color = forms.ModelChoiceField(queryset=Color.objects.all(), required=True, help_text='Required')
    manufacturer = forms.ModelChoiceField(queryset=Manufacturer.objects.all(), required=True, help_text='Required')
    is_special = forms.BooleanField(required=False)

    class Meta:
        model = Car

    # !!!!!!!!!!!!!!!!! review filtering inputs
    def save(self, request):
        car = Car()
        car.name = clean_input(self.cleaned_data['name'])
        car.cost = self.cleaned_data['cost']

        # if the field is equal None then don't proceed clean_input
        if self.cleaned_data['image'] is not None:
            car.image = self.cleaned_data['image']
        else:
            car.image = None
        car.engine_capacity = self.cleaned_data['engine_capacity']
        car.color = self.cleaned_data['color']
        car.manufacturer = self.cleaned_data['manufacturer']
        car.is_special = self.cleaned_data['is_special']
        car.save()
        return {"type": "success", "message": 'The car was added.'}

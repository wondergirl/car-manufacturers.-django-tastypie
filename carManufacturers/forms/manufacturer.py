__author__ = 'ann'
from django import forms

from django.forms.extras.widgets import SelectDateWidget

from carManufacturersApp.models import Country
from carManufacturersApp.models import Manufacturer
from carManufacturers.input_helpers import clean_input


class AddManufacturerForm(forms.Form):
    name = forms.CharField(max_length=100, required=True, help_text='Required')
    founding_date = forms.DateField(widget=SelectDateWidget(years=range(1947, 2016)), required=False)
    country = forms.ModelChoiceField(queryset=Country.objects.all(), required=True, help_text='Required')
    website = forms.URLField(help_text='Format: http://example.com')

    class Meta:
        model = Manufacturer

    # !!!!!!!!!!!!!!!!! review filtering inputs
    def save(self, request):
        manufacturer = Manufacturer()
        manufacturer.name = clean_input(self.cleaned_data['name'])

        # if the field is equal None then don't proceed clean_input
        if self.cleaned_data['founding_date'] is not None:
            manufacturer.founding_date = clean_input(self.cleaned_data['founding_date'])
        else:
            manufacturer.founding_date = None
        manufacturer.country = self.cleaned_data['country']
        manufacturer.website = clean_input(self.cleaned_data['website'])
        manufacturer.save()
        return {"type": "success", "message": 'The manufacturer was added.'}

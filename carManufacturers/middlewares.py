__author__ = 'ann'
# coding=utf-8

import datetime
import re
import logging
from django.conf import settings

logger = logging.getLogger(__name__)


class ResponseLoggingMiddleware(object):
    def process_response(self, request, response):
        if not re.search(settings.API_URL, request.get_full_path()):
            return response

        logger.debug(
            "[{request_method}] {url} {date_time} {status}".format(url=request.get_full_path(),
                                                                   request_method=request.method,
                                                                   date_time=datetime.datetime.now(),
                                                                   status=response.status_code)
        )

        return response

from django.conf.urls import include, url
from django.conf import settings
from tastypie.api import Api

from carManufacturersApp.api import CarResource, ColorResource, ManufacturerResource
from carManufacturersApp.views import home, manufacturers_list, add_manufacturer, add_car, add_client


# API register routes
v1_api = Api()
v1_api.register(ColorResource())
v1_api.register(CarResource())
v1_api.register(ManufacturerResource())


from django.contrib import admin
admin.autodiscover()


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', home, name='home'),
    url(r'^manufacturers', manufacturers_list, name='manufacturers'),
    url(r'^add-manufacturer', add_manufacturer, name='add-manufacturer'),
    url(r'^add-car', add_car, name='add-car'),

    url(r'^api/', include(v1_api.urls)),

    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^add-client/', add_client, name='add-client')
]


# Serving files from MEDIA_URL is dangerous in production environment
if settings.DEBUG:
    urlpatterns += [
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
    ]

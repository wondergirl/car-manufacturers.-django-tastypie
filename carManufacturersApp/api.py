__author__ = 'ann'

from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.utils import simplejson
from django.utils.cache import patch_cache_control
from django.views.decorators.csrf import csrf_exempt

from tastypie.resources import ModelResource
from tastypie.authorization import DjangoAuthorization
from tastypie_oauth.authentication import OAuth20Authentication
from tastypie import fields
from tastypie.paginator import Paginator
from tastypie import http
from tastypie.exceptions import BadRequest
from tastypie.validation import FormValidation, Validation
from tastypie.throttle import BaseThrottle

from carManufacturersApp.models import Car, Color, Manufacturer


class ColorResource(ModelResource):
    class Meta:
        queryset = Color.objects.all()
        resource_name = 'color'



class ManufacturerResource(ModelResource):
    class Meta:
        queryset = Manufacturer.objects.all()
        resource_name = 'manufacturer'


class CarResource(ModelResource):
    color = fields.ForeignKey(ColorResource, 'color', full=True)
    manufacturer = fields.ForeignKey(ManufacturerResource, 'manufacturer', full=True)

    class Meta:
        queryset = Car.objects.all()
        resource_name = 'car'
        allowed_methods = ['get', 'post']
        authorization = DjangoAuthorization()
        authentication = OAuth20Authentication()
        validation = Validation()
        always_return_data = True
        paginator_class = Paginator
        throttle = BaseThrottle(throttle_at=1, timeframe=10)

    def wrap_view(self, view):

        @csrf_exempt
        def wrapper(request, *args, **kwargs):
            try:
                callback = getattr(self, view)

                response = callback(request, *args, **kwargs)

                if request.is_ajax():
                    patch_cache_control(response, no_cache=True)

                if request.META['REQUEST_METHOD'] == 'POST':
                    lst_dic = [dict(success=True)]
                    response = HttpResponse(simplejson.dumps(lst_dic), content_type='application/json')
                return response
            except (BadRequest, fields.ApiFieldError), e:
                return http.HttpBadRequest({'success': False, 'code': 666, 'message': e.args[0]})
            except ValidationError, e:
                return http.HttpBadRequest({'success': False, 'code': 777, 'message': ', '.join(e.messages)})
            except Exception, e:
                return self._handle_500(request, e)

        return wrapper

    def search(self, request, **kwargs):
        self.throttle_check(request)

# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='car',
            old_name='engineCapacity',
            new_name='engine_capacity',
        ),
        migrations.RenameField(
            model_name='manufacturer',
            old_name='foundingDate',
            new_name='founding_date',
        ),
    ]

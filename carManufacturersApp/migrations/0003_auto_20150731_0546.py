# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0002_auto_20150730_2132'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='manufacturer',
            field=models.ForeignKey(to='carManufacturersApp.Manufacturer'),
            preserve_default=True,
        ),
    ]

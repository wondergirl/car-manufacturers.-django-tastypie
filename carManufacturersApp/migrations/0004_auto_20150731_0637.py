# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0003_auto_20150731_0546'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='img',
            field=models.ImageField(upload_to=b'cars'),
            preserve_default=True,
        ),
    ]

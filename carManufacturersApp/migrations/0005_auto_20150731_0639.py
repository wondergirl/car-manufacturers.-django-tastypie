# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0004_auto_20150731_0637'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='manufacturercar',
            name='car',
        ),
        migrations.RemoveField(
            model_name='manufacturercar',
            name='manufacturer',
        ),
        migrations.DeleteModel(
            name='ManufacturerCar',
        ),
    ]

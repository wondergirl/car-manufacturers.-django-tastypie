# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0010_auto_20150731_0701'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='cost2',
            field=models.FloatField(default=0),
            preserve_default=True,
        ),
    ]

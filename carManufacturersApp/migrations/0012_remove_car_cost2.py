# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0011_car_cost2'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='car',
            name='cost2',
        ),
    ]

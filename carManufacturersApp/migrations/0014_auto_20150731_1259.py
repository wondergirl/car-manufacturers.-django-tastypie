# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0013_auto_20150731_0715'),
    ]

    operations = [
        migrations.AlterField(
            model_name='manufacturer',
            name='founding_date',
            field=models.DateField(),
            preserve_default=True,
        ),
    ]

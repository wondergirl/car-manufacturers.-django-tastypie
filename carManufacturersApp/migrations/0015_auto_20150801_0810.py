# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):
    dependencies = [
        ('carManufacturersApp', '0014_auto_20150731_1259'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='is_special',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='manufacturer',
            name='website',
            field=models.URLField(default=None),
            preserve_default=True,
        ),
    ]

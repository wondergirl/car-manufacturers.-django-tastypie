__author__ = 'ann'
from django.db import models

from carManufacturersApp.models.manufacturer import Manufacturer
from carManufacturersApp.models.color import Color


class Car(models.Model):
    name = models.CharField(max_length=100, null=False)
    cost = models.FloatField(null=False)
    image = models.ImageField(upload_to='cars')
    engine_capacity = models.IntegerField(null=False)
    color = models.ForeignKey(Color, null=False)
    manufacturer = models.ForeignKey(Manufacturer, null=False)  # Assume that a car can have only one manufacturer
    is_special = models.BooleanField(default=False)

__author__ = 'ann'

from django.db import models


class Color(models.Model):
    name = models.CharField(max_length=30, null=False)

    def __unicode__(self):
        return u'{0}'.format(self.name)

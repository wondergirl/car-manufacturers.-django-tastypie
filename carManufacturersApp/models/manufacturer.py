__author__ = 'ann'

from django.db import models

from carManufacturersApp.models.country import Country


class Manufacturer(models.Model):
    name = models.CharField(max_length=100, null=False)
    founding_date = models.DateField()
    country = models.ForeignKey(Country, null=False)
    website = models.URLField(default=None)

    def __unicode__(self):
        return u'{0}'.format(self.name)

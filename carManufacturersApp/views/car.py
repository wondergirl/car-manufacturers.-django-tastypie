__author__ = 'ann'

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from carManufacturers.forms import AddCarForm


def add_car(request):
    if request.method == 'POST':
        form = AddCarForm(request.POST, request.FILES)
        if form.is_valid():
            form.save(request)
            messages.add_message(request, messages.INFO, 'The manufacturer was added')
            return HttpResponseRedirect(reverse('home'))
    else:
        form = AddCarForm()

    return render_to_response(
        "car/base_add_car.html", {"form": form},
        context_instance=RequestContext(request)
    )

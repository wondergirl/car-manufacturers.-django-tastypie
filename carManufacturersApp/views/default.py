__author__ = 'ann'
from django.shortcuts import render_to_response
from django.template import RequestContext

from carManufacturersApp.models import Car


def home(request):
    cars = Car.objects.all()

    return render_to_response(
        "car/base_list_cars.html", {"cars": cars},
        context_instance=RequestContext(request)
    )

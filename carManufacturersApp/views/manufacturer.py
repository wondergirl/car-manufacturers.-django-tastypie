__author__ = 'ann'

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

from carManufacturersApp.models import Manufacturer
from carManufacturers.forms import AddManufacturerForm


def manufacturers_list(request):
    manufacturers = Manufacturer.objects.all()

    return render_to_response(
        "manufacturer/base_list_manufacturers.html", {"manufacturers": manufacturers},
        context_instance=RequestContext(request)
    )


def add_manufacturer(request):
    if request.method == 'POST':
        form = AddManufacturerForm(request.POST)
        if form.is_valid():
            form.save(request)
            messages.add_message(request, messages.INFO, 'The manufacturer was added')
            return HttpResponseRedirect(reverse('manufacturers'))
    else:
        form = AddManufacturerForm()

    return render_to_response(
        "manufacturer/base_add_manufacturer.html", {"form": form},
        context_instance=RequestContext(request)
    )

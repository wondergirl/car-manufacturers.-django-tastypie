CREATE DATABASE  IF NOT EXISTS `projectDb` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `projectDb`;
-- MySQL dump 10.13  Distrib 5.5.43, for debian-linux-gnu (i686)
--
-- Host: 127.0.0.1    Database: projectDb
-- ------------------------------------------------------
-- Server version	5.5.43-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group_permissions_group_id_58c48ba9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group_permissi_permission_id_23962d04_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth_permissi_content_type_id_51277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add color',7,'add_color'),(20,'Can change color',7,'change_color'),(21,'Can delete color',7,'delete_color'),(22,'Can add country',8,'add_country'),(23,'Can change country',8,'change_country'),(24,'Can delete country',8,'delete_country'),(25,'Can add manufacturer',9,'add_manufacturer'),(26,'Can change manufacturer',9,'change_manufacturer'),(27,'Can delete manufacturer',9,'delete_manufacturer'),(28,'Can add car',10,'add_car'),(29,'Can change car',10,'change_car'),(30,'Can delete car',10,'delete_car');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$12000$KjtJoUoqxwgt$kGO6T0N2ou8q1Rx96JKdfJSp4llhR4L/g61ZlTtr9uE=','2015-07-31 07:02:24',1,'ann','','','',1,1,'2015-07-31 07:02:24');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_30a071c9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_24702650_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_user_permissions_user_id_7cd7acb6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_user_perm_permission_id_3d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carManufacturersApp_car`
--

DROP TABLE IF EXISTS `carManufacturersApp_car`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carManufacturersApp_car` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `cost` double NOT NULL,
  `engine_capacity` int(11),
  `color_id` int(11) NOT NULL,
  `manufacturer_id` int(11) NOT NULL,
  `image` varchar(45) NOT NULL,
  `is_special` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `carManufacturersApp_car_399a0583` (`color_id`),
  KEY `carManufacturersApp_car_4d136c4a` (`manufacturer_id`),
  CONSTRAINT `carManufacture_color_id_39f12c71_fk_carManufacturersApp_color_id` FOREIGN KEY (`color_id`) REFERENCES `carManufacturersApp_color` (`id`),
  CONSTRAINT `manufacturer_id_11563e48_fk_carManufacturersApp_manufacturer_id` FOREIGN KEY (`manufacturer_id`) REFERENCES `carManufacturersApp_manufacturer` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carManufacturersApp_car`
--

LOCK TABLES `carManufacturersApp_car` WRITE;
/*!40000 ALTER TABLE `carManufacturersApp_car` DISABLE KEYS */;
INSERT INTO `carManufacturersApp_car` VALUES (2,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(3,'Mercedez Benz A6',1500,1000,2,1,'car2.jpg',0),(4,'dsds',1200,1233,2,3,'',0),(5,'Audi A7',2500,12234,3,3,'cars/1.jpg',0),(6,'Audi A3',2000,2332,3,3,'cars/1_VKftjBV.jpg',0),(7,'Audi A3',1000,1212,3,3,'cars/fonstola.ru-82354.jpg',1),(8,'test',2121,213232,3,3,'cars/1_WSskdT5.jpg',0),(9,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(10,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(11,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(12,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(13,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(14,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(15,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(16,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(17,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(18,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(19,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(20,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(21,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(22,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(23,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(24,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(25,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(26,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(27,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(28,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(29,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(30,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(31,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(32,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(33,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(34,'Audi A7',787,888,3,3,'',0),(35,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(36,'Mercedez Benz A5',1500,1000,1,3,'/media/car1.jpg',0),(37,'',1500,1000,1,3,'/media/car1.jpg',0);
/*!40000 ALTER TABLE `carManufacturersApp_car` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carManufacturersApp_color`
--

DROP TABLE IF EXISTS `carManufacturersApp_color`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carManufacturersApp_color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carManufacturersApp_color`
--

LOCK TABLES `carManufacturersApp_color` WRITE;
/*!40000 ALTER TABLE `carManufacturersApp_color` DISABLE KEYS */;
INSERT INTO `carManufacturersApp_color` VALUES (1,'red'),(2,'green'),(3,'blue'),(4,'pink');
/*!40000 ALTER TABLE `carManufacturersApp_color` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carManufacturersApp_country`
--

DROP TABLE IF EXISTS `carManufacturersApp_country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carManufacturersApp_country` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carManufacturersApp_country`
--

LOCK TABLES `carManufacturersApp_country` WRITE;
/*!40000 ALTER TABLE `carManufacturersApp_country` DISABLE KEYS */;
INSERT INTO `carManufacturersApp_country` VALUES (1,'Germany'),(2,'Belgium');
/*!40000 ALTER TABLE `carManufacturersApp_country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `carManufacturersApp_manufacturer`
--

DROP TABLE IF EXISTS `carManufacturersApp_manufacturer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `carManufacturersApp_manufacturer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `founding_date` date,
  `country_id` int(11) NOT NULL,
  `website` varchar(200) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `carManufacturersApp_manufacturer_93bfec8a` (`country_id`),
  CONSTRAINT `carManufac_country_id_51636500_fk_carManufacturersApp_country_id` FOREIGN KEY (`country_id`) REFERENCES `carManufacturersApp_country` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `carManufacturersApp_manufacturer`
--

LOCK TABLES `carManufacturersApp_manufacturer` WRITE;
/*!40000 ALTER TABLE `carManufacturersApp_manufacturer` DISABLE KEYS */;
INSERT INTO `carManufacturersApp_manufacturer` VALUES (1,'Mercedez Benz ','1965-03-02',1,''),(2,'dsds','1980-04-01',1,''),(3,'Crysler','1980-01-01',2,''),(4,'BMW','1992-08-16',1,''),(7,'&lt;!DOCTYPE html&gt; &lt;html&gt; &lt;head&gt; &lt;style&gt;','1951-03-04',2,''),(8,'&lt;!DOCTYPE html&gt; &lt;html&gt; &lt;head&gt; &lt;style&gt;',NULL,1,''),(9,'&lt;!DOCTYPE html&gt; &lt;html&gt; &lt;head&gt; &lt;style&gt;',NULL,1,''),(10,'&lt;!DOCTYPE html&gt; &lt;html&gt; &lt;head&gt; &lt;style&gt;','1956-06-07',1,''),(14,'test',NULL,1,'http://www.example.com/'),(15,'dsds',NULL,1,'http://www.example.com/');
/*!40000 ALTER TABLE `carManufacturersApp_manufacturer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_1c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `django_admin__content_type_id_5151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_3ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'user','auth','user'),(5,'content type','contenttypes','contenttype'),(6,'session','sessions','session'),(7,'color','carManufacturersApp','color'),(8,'country','carManufacturersApp','country'),(9,'manufacturer','carManufacturersApp','manufacturer'),(10,'car','carManufacturersApp','car');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-07-30 20:39:56'),(2,'auth','0001_initial','2015-07-30 20:39:59'),(3,'admin','0001_initial','2015-07-30 20:40:00'),(4,'sessions','0001_initial','2015-07-30 20:40:00'),(5,'carManufacturersApp','0001_initial','2015-07-30 21:25:43'),(6,'carManufacturersApp','0002_auto_20150730_2132','2015-07-30 21:33:03'),(7,'carManufacturersApp','0003_auto_20150731_0546','2015-07-31 05:50:55'),(8,'carManufacturersApp','0004_auto_20150731_0637','2015-07-31 06:38:34'),(9,'carManufacturersApp','0005_auto_20150731_0639','2015-07-31 06:50:24'),(10,'carManufacturersApp','0006_car_image','2015-07-31 06:50:24'),(11,'carManufacturersApp','0007_remove_car_img','2015-07-31 06:52:50'),(12,'carManufacturersApp','0008_remove_car_image','2015-07-31 06:59:17'),(13,'carManufacturersApp','0009_car_image','2015-07-31 06:59:17'),(14,'carManufacturersApp','0010_auto_20150731_0701','2015-07-31 07:01:45'),(15,'carManufacturersApp','0011_car_cost2','2015-07-31 07:11:10'),(16,'carManufacturersApp','0012_remove_car_cost2','2015-07-31 07:11:51'),(17,'carManufacturersApp','0013_auto_20150731_0715','2015-07-31 07:27:07'),(18,'carManufacturersApp','0014_auto_20150731_1259','2015-07-31 12:59:54'),(19,'carManufacturersApp','0015_auto_20150801_0810','2015-08-01 08:10:21');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('94tg8o3oopk9eikkh54mkj4n656m7flv','NWIyODcyMTNmZmExYmNhZjUyNzgxYzMyZTU3MmVmMTRhOWEzNGZkNDp7fQ==','2015-08-14 05:58:44'),('kjfl9hk1kqie0g1cg6jvf48chgi19x20','NWIyODcyMTNmZmExYmNhZjUyNzgxYzMyZTU3MmVmMTRhOWEzNGZkNDp7fQ==','2015-08-15 07:59:20');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-02 18:35:19
